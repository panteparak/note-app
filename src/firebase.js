import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAsjhkYlS0aPLgeA1uy5EcdtZsJpg0cNtc',
  authDomain: 'login-demo-bc5f6.firebaseapp.com',
  databaseURL: 'https://login-demo-bc5f6.firebaseio.com',
  projectId: 'login-demo-bc5f6',
  storageBucket: 'login-demo-bc5f6.appspot.com',
  messagingSenderId: '994437964619',
};
firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();