import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/components/App';
import BrowserRouter from 'react-router-dom/BrowserRouter'

import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

const theme = createMuiTheme({

})

const Main = () => (
    <MuiThemeProvider theme={theme}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </MuiThemeProvider>
);

ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorker();
