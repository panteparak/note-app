import React, { Component } from 'react';
import compose from "recompose/compose";
import {Redirect, Route, withRouter} from 'react-router-dom';
import ContextProvider from "src/context/withContext";
import Login from "./Login";
import Signup from "./Signup";
import _ from "lodash";
import Main from "./Main";
import firebase, { auth, db } from 'src/firebase';
import ForgetPassword from "./ForgetPassword";
import { CircularProgress } from 'material-ui/Progress';
import Bar from "./Bar";

const AuthenticatedRoute = ({ component: Component, auth, ...rest }) => (

        <Route {...rest} render={
            props =>
                auth === true ?
                    (<Bar><Component {...props} {...rest} /></Bar>) : (<Redirect to="/login"/>)
            }
        />
);

class App extends Component {

    state = {
        authenticated: false,
        currentUser: null,
        verification: null,
        displayName: null,
        loading: true,
        photoURL: null,
        firebase: firebase,
        auth: auth,
        db: db
    };

    componentWillMount() {
        // const {firebase: { auth }} = this.state;

        const {auth} = this.state;

        const getDisplayName = (user) => {
            return user.displayName === null ? user.email : user.displayName;
        }

        const providerLogin = (user, cb) => {
            this.setState(
                {
                    authenticated: true,
                    currentUser: user,
                    verification: true,
                    loading: false,
                    photoURL: user.photoURL,
                    displayName: getDisplayName(user)

                }, cb);
        };

        const emailLogin = (user, cb, errCb) => {
            if (user.emailVerified) {
                this.setState({
                    authenticated: true,
                    currentUser: user,
                    verification: true,
                    loading: false,
                    photoURL: user.photoURL,
                    displayName: getDisplayName(user)
                }, cb);
            } else {
                this.setState({
                    authenticated: false,
                    currentUser: null,
                    verification: false,
                    loading: false,
                    photoURL: null,
                    displayName: null
                }, errCb)
            }
        };

        auth.onAuthStateChanged(user => {
            if (user){
                if (this.isEmailLogin(user)){
                    emailLogin(user,
                        () => this.props.history.push("/"),
                        () => this.props.history.push("/login")
                    );

                } else if (this.isSSO(user)){
                    providerLogin(user,
                        () => this.props.history.push("/")
                    );
                }
            } else {
                this.setState({
                    authenticated: false,
                    loading: false,
                    currentUser: null,
                    verification: false,
                    displayName: null
                }, () => this.props.history.push('/login'))
            }
        })
    }

    isEmailLogin = (data) =>  data.providerData[0].providerId === "password";
    isSSO = (data) => data.providerData[0].providerId === "facebook.com" || data.providerData[0].providerId === "google.com";

    updateState = (nextState, cb) => {
        this.setState({
            ..._.pick(nextState, Object.keys(this.state))
        }, cb)
    };

    render(){
        const { authenticated, loading } = this.state;

        if (loading){
            return (
                <CircularProgress size={80} thickness={4} />
            )
        }

        return (
            <ContextProvider state={{ context: this.state, setContext: this.updateState }} >
                <Route exact path="/login" component={Login}/>
                <AuthenticatedRoute exact path="/" component={Main} auth={authenticated} />
                <Route exact path="/signup" component={Signup} />
                <Route exact path={"/resetpassword"} component={ForgetPassword} />
            </ContextProvider>
        )
    }
}

export default compose(
    withRouter,
) (App);