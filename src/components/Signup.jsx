import React, {Component} from 'react';
import compose from "recompose/compose";
import PropTypes from 'prop-types'
import {withStyles} from "material-ui/styles";
import TextField from 'material-ui/TextField';
import pure from "recompose/pure";
import Background from "src/components/Background";
import {withContext} from "src/context/withContext";
import Button from "material-ui/Button";
import Slide from 'material-ui/transitions/Slide';
import _ from "lodash";
import {FlexContainer, FlexItem} from "./Flex";
import Dialog, {DialogTitle, DialogContent, DialogContentText, DialogActions} from "material-ui/Dialog";
import firebase, {auth} from 'src/firebase'

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

const styles = theme => ({
    root: {

    },
    textField: {

    },
    button: {
        margin: theme.spacing.unit,
        flexGrow: 1,
    }
});

const ConfirmDialog = ({ classes, handleClose, open, transition=Transition, email, success=true, error }) => {

    let heading;
    let subOne;
    let subTwo;

    if (success){
        heading = "Email Verification Sent";
        subOne = "Before you are able to login.....";
        subTwo = `A confirmation email has been sent to ${email}, please verify your email address.`;
    } else {
        heading = "Failed to send Email verification";
        subOne = "Before you go....";
        subTwo = error;
    }

    return (
        <Dialog
            open={open}
            transition={transition}
            keepMounted
            onClose={handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">
                {heading}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    {subOne}
                </DialogContentText>
                <DialogContentText id="alert-dialog-slide-description">
                    {subTwo}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    )
};

const InputTextField = withStyles(styles)(({ onChange, value, classes, label }) => (
    <FlexItem>
        <TextField
            label={label}
            className={classes.textField}
            value={value}
            onChange={onChange}
        />
    </FlexItem>
));

InputTextField.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.node.isRequired,
    label: PropTypes.node.isRequired
};

const SecretInputTextField =  withStyles(styles)(({ onChange, value, classes, label }) => (
    <FlexItem>
        <TextField
            label={label}
            className={classes.textField}
            value={value}
            onChange={onChange}
            type={"password"}
        />
    </FlexItem>
));


SecretInputTextField.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.node.isRequired,
    label: PropTypes.node.isRequired
};

const SubmitButton = withStyles(styles)(({classes, onClick, value}) => (
    <FlexItem>
        <Button variant="raised" color="primary" className={classes.button} onClick={onClick}>
            {value}
        </Button>
    </FlexItem>
));


class SignUp extends Component {

    static propTypes = {

    };
    state = {
        email: "",
        password: "",
        repeatpassword: "",
        firstname: "",
        lastname: "",
        open: false,
        success: true,
        failureMessage: ""
    };
    handleSuccessDialogOpen = () => {
        this.setState({ open: true, success: true, failureMessage: "" });
    };


    handleSuccessDialogClose = () => {
        this.setState({ open: false, success: true, failureMessage: "" });
    };

    handleFailureDialogOpen = (message) => () => {
        this.setState({
            open: true,
            success: true,
            failureMessage: message
        })
    };


    handleChange = (name) => (e) => {
        this.setState({
            [name]: e.target.value
        })
    };


    handleEmailSignUp = () => {

        const {email, password, repeatpassword} = this.state;


        if (_.trim(email).length === 0){
            alert("blank username");
            return;
        }

        if (_.trim(password).length === 0  || _.trim(repeatpassword).length === 0 || password !== repeatpassword) {
            alert("password not match or empty");
            return;
        }

        auth.createUserWithEmailAndPassword(email, password)
            .then(user => user.sendEmailVerification())
            .then(() => this.handleSuccessDialogOpen())
            .catch(authError => this.handleFailureDialogOpen(authError));

    };

    render(){

        const {email, password, repeatpassword, open, success} = this.state;
        const { classes } = this.props;

        return (
            <Background>
                <FlexContainer>
                    <InputTextField label="Email" value={email} onChange={this.handleChange("email")}/>
                    <SecretInputTextField label={"Password"} value={password} onChange={this.handleChange("password")} />
                    <SecretInputTextField label={"Repeat Password"} value={repeatpassword} onChange={this.handleChange("repeatpassword")} />
                </FlexContainer>
                <FlexContainer>
                    <SubmitButton value={"Sign Up"} onClick={this.handleEmailSignUp}/>
                </FlexContainer>
                <ConfirmDialog
                    handleClose={this.handleSuccessDialogClose}
                    open={open} transition={Transition}
                    email={email}
                    success={success} />
            </Background>
        )
    }
}

export default compose(
    withStyles(styles),
    withContext
)(SignUp);