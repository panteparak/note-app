import React, {Component} from 'react';
import PropTypes from "prop-types";
import {withStyles} from "material-ui/styles";
import TextField from "material-ui/TextField";
import Button from "material-ui/Button";
import {FlexItem} from "./Flex";
import compose from 'recompose/compose'
import {withContext} from 'src/context/withContext'
import Background from "./Background";

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
        flexGrow: 1,
    },
    textField: {

    },
});

const InputTextField = withStyles(styles)(({ onChange, value, classes, label }) => (
    <FlexItem>
        <TextField
            label={label}
            className={classes.textField}
            value={value}
            onChange={onChange}
        />
    </FlexItem>
));

InputTextField.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.node.isRequired,
    classes: PropTypes.object.isRequired,
    label: PropTypes.node.isRequired
};

const SubmitButton = withStyles(styles)(({classes, onClick, value}) => (
    <FlexItem>
        <Button variant="raised" color="primary" className={classes.button} onClick={onClick}>
            {value}
        </Button>
    </FlexItem>
));

class ForgetPassword extends Component {

    state = {
        email: ""
    };

    static propTypes = {

    };

    handleChange = (name) => (e) => {
        this.setState({
            [name]: e.target.value,
        })
    };

    handleForgetPassword = () => {
        const {email} = this.state;
        const {auth} = this.props.context;

        auth.sendPasswordResetEmail(email)
            .then(() => {
                alert("email send")
            }).catch((error) => {
                console.log(error)
            });
    };

    render(){
        const {email} = this.state;
        const { classes } = this.props;
        return (
            <Background>
                <TextField value={email} onChange={this.handleChange('email')} label={'Email'} />
                <SubmitButton onClick={this.handleForgetPassword} classes={classes} value={"Reset Password"}/>
            </Background>

        )
    }

}

export default compose(
    withStyles(styles),
    withContext
)(ForgetPassword);