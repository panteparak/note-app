import React, {Component} from 'react';
import compose from "recompose/compose";
import pure from "recompose/pure";
import {withContext} from "../context/withContext";
import {CircularProgress} from "material-ui/Progress";
import { withStyles } from "material-ui/styles";
import List, {ListItem, ListItemSecondaryAction, ListItemText} from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from 'material-ui/TextField'
import Button from "material-ui/Button";
import Background from "./Background";

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
});

const NoteItem = ({ value, onDelete, classes}) => (
    <ListItem button>
        <ListItemText primary={value}/>
        <ListItemSecondaryAction>
            <IconButton aria-label="Delete" onClick={onDelete}>
                <DeleteIcon />
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
);

const Note = ({data, classes, children, onDelete}) => {
    if (data === null){
        return <CircularProgress className={classes.progress} />
    }

    return (
        <List component="nav">
            {
                data.map((d, i) => (<NoteItem key={i} value={d} onDelete={onDelete(i)}/>))
            }
        </List>
    )
};

const InputField = ({className, value, onChange, label, ...rest}) => (
    <TextField
        label={label}
        className={className}
        value={value}
        onChange={onChange}
        margin="normal"
    />
);


class Main extends Component {

    state = {
        data: null,
        noteField: ''
    };

    componentDidMount(){

        const {db, currentUser: {uid}} = this.props.context;
        const noteRef = db.ref(`${uid}/notes`);

        this.setState({
            data: [],
        }, () => {
            noteRef.on('child_added', note => {
                this.setState((prevState) => ({
                    data: [...prevState.data, note.val()]
                }))
            });
        })

    }

    addNote = () => {
        const {db, currentUser: {uid}} = this.props.context;
        const {noteField: note} = this.state
        const noteRef = db.ref(`${uid}/notes`);

        noteRef.transaction(currentData => {
            if (currentData === null){
                return [note]
            } else {
                return [...currentData, note]
            }
        })
    };

    updateNote = (notes) => {
        const {db, currentUser: {uid}} = this.props.context;
        const noteRef = db.ref(`${uid}/notes`);
        noteRef.transaction(prevNote => {
            if (prevNote){
                return [...notes];
            }
            return [];
        })
    };

    deleteNote = (i) => () => {
        const {data: notes} = this.state;
        notes.splice(i, 1);

        this.setState({
            data: [...notes]
        });
        this.updateNote(notes)
    };

    handleStateChange = (name) => (e) => {
        this.setState({
            [name]: e.target.value,
        })
    };

    static propTypes = {

    };

    render(){
        const {classes} = this.props;
        const {data, noteField} =  this.state;

        return (
            <Background>
                <Note data={data} classes={classes} onDelete={this.deleteNote}/>
                <InputField className={classes.textField} onChange={this.handleStateChange('noteField')} value={noteField} label="Add New Note..." />
                <Button onClick={this.addNote}>Add Note</Button>
            </Background>
        )
    }

}

export default compose(
    withStyles(styles),
    withContext
) (Main);