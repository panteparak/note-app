import React from 'react'
import {withStyles} from "material-ui/styles/index";
import Grid from "material-ui/Grid";

const flexContainerStyle = theme => ({

});

export const FlexContainer = withStyles(flexContainerStyle)(({children, classes}) => (
    <Grid container className={classes.root}>
        {children}
    </Grid>
));

export const FlexItem = ({children, xs=12, sm, md}) => (
    <Grid item xs={xs} sm={sm} md={md}>
        {children}
    </Grid>
);