
import compose from "recompose/compose";
import { withContext } from "src/context/withContext";

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Menu, { MenuItem } from 'material-ui/Menu';
import Dialog from "material-ui/Dialog/Dialog";
import DialogTitle from "material-ui/Dialog/DialogTitle";
import DialogContent from "material-ui/Dialog/DialogContent";
import DialogActions from "material-ui/Dialog/DialogActions";
import Button from "material-ui/Button/Button";
import TextField from "material-ui/TextField";
import Grid from "material-ui/Grid";
import FaceIcon from '@material-ui/icons/Face';
import Avatar from "material-ui/Avatar/Avatar";
import Chip from "material-ui/Chip/Chip";

const styles = (theme) => ({
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    chip: {
        margin: theme.spacing.unit,
    },


});


const InputTextField = ({password=false, className, value, onChange, label}) => (
    <TextField
        value={value}
        type={password ? 'password': 'text'}
        className={className}
        onChange={onChange}
        label={label}/>

);

const SubmitButton = ({value, className, variant="raised", onClick}) => (
    <Button variant={variant} className={className} onClick={onClick}>
        {value}
    </Button>
);

const UpdateDataDialog = ({anchorEl, open, handleClose, handleNameChange, handleEmailChange, handlePasswordChange, handleNameSubmit, handleEmailSubmit, handlePasswordSubmit}) => (
    <Dialog
        open={open}
        onClose={this.handleDialogClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">User Profile</DialogTitle>
        <DialogContent>
            <Grid container spacing={16}>
                <Grid item xs={8}>
                    <InputTextField onChange={handleNameChange} label={"Name"}/>
                </Grid>
                <Grid item xs={8}>
                    <SubmitButton onClick={handleNameSubmit} value={"Update Name"}/>
                </Grid>
            </Grid>
            <Grid container spacing={16}>
                <Grid item xs={8}>
                    <InputTextField onChange={handleEmailChange} label={"E-Mail"} />
                </Grid>

                <Grid item xs={8}>
                    <SubmitButton onClick={handleEmailSubmit} value={"Update Email"} />
                </Grid>
            </Grid>

            <Grid container spacing={8}>
                <Grid item xs={8}>
                    <SubmitButton onClick={handlePasswordSubmit} value={"Reset Password"} />
                </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose} color="primary" autoFocus>
                Close
            </Button>
        </DialogActions>
    </Dialog>
)

class MenuAppBar extends React.Component {

    state = {
        anchorEl: null,
        openDialog: false,
        name: "",
        email: "",
        password: ""

    };

    static propTypes = {
        classes: PropTypes.object.isRequired,
    }

    handleStateChange = (name) => (e) => {
        this.setState({
            [name]: e.target.value
        })
    }

    handleChange = (event, checked) => {
        this.setState({ auth: checked });
    };

    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    handleDialogOpen = () => {
        this.setState({openDialog: true})
    }

    handleDialogClose = () => {
        this.setState({ openDialog: false });
    };

    handleLogout = () => {
        const {auth} = this.props.context;
        auth.signOut();
        this.handleClose();
    }

    handleProfile = () => {
        this.handleDialogOpen();
        this.handleClose();
    }

    handleNameSubmit = () => {
        const {name} = this.state;
        const {setContext, context: {auth}} = this.props;
        console.log(auth.currentUser);


        auth.currentUser.updateProfile({ displayName: name })
            .then(() => {
                setContext({
                    displayName: name
                });
            })
            .catch(authError => {
                alert(authError.message);
            })
    };



    handleEmailSubmit = () => {
        const {email} = this.state;
        const {setContext, context: {auth}} = this.props;

        auth.currentUser.updateEmail(email)
            .then(() => auth.currentUser.sendEmailVerification())
            .then(() => alert(`Email updated to ${email}`))
            .then(() => auth.signOut())
            .catch((err) => alert(err.message));
    };


    handlePasswordReset = () => {

        const {context: {auth: {currentUser: user}}} = this.props;

        this.props.context.auth.sendPasswordResetEmail(user.email)
            .then(() => {
                alert(`Password Reset Email sent to ${user.email}`)
            })
            .catch((err) => alert(err.message));
    };



    render() {
        const { classes, children, context: {photoURL, displayName} } = this.props;
        const { anchorEl, openDialog } = this.state;
        const open = Boolean(anchorEl);

        const avatar = photoURL === null ? (<Avatar><FaceIcon /></Avatar>) : (<Avatar src={photoURL} />);


        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="title" color="inherit" className={classes.flex} />
                            <div>
                                <Chip
                                    avatar={avatar}
                                    label={displayName}
                                    onClick={this.handleMenu}
                                    className={classes.chip}
                                />
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem onClick={this.handleProfile}>Profile</MenuItem>
                                    <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                                </Menu>
                            </div>
                    </Toolbar>
                </AppBar>
                {children}
                <UpdateDataDialog
                    handleClose={this.handleDialogClose}
                    anchorEl={anchorEl} open={openDialog}
                    handleNameChange={this.handleStateChange('name')}
                    handleEmailChange={this.handleStateChange('email')}
                    handlePasswordChange={this.handleStateChange('password')}
                    handleNameSubmit={this.handleNameSubmit}
                    handleEmailSubmit={this.handleEmailSubmit}
                    handlePasswordSubmit={this.handlePasswordReset}
                />


            </div>
        );
    }
}

export default compose(
    withStyles(styles),
    withContext
) (MenuAppBar);