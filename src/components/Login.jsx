import React, {Component} from 'react';
import compose from "recompose/compose";
import {withStyles} from "material-ui/styles";
import TextField from "material-ui/TextField";
import Grid from "material-ui/Grid";
import pure from 'recompose/pure';
import {withContext} from "src/context/withContext";
import Button from 'material-ui/Button';
import Background from "./Background";
import {FlexItem} from "./Flex";
import Dialog, {DialogTitle, DialogContent, DialogContentText, DialogActions} from "material-ui/Dialog";
import Slide from "material-ui/transitions/Slide";
import firebase, {auth} from 'src/firebase'


function Transition(props) {
    return <Slide direction="up" {...props} />;
}

const styles = theme => ({
    root: theme.mixins.gutters({
        padding: theme.spacing.unit * 5,
        margin:  theme.spacing.unit * 3,
    }),
    grid: {
        flexGrow: 1,
    },
    margin: {

    },
    textField: {

    },
    button: {
        margin: theme.spacing.unit,
        flexGrow: 1,
    },
});

const GridContainer = withStyles(styles)(({children, classes}) => (
    <Grid container className={classes.root}>
        {children}
    </Grid>
));

const UsernameField = withStyles(styles)(({ onChange, value, classes }) => (
    <FlexItem>
        <TextField
            label="Email"
            className={classes.textField}
            value={value}
            onChange={onChange}
        />
    </FlexItem>
));


const PasswordField =  withStyles(styles)(({ onChange, value, classes }) => (
    <FlexItem>
        <TextField
            label={"Password"}
            type={"password"}
            className={classes.textField}
            value={value}
            onChange={onChange} />
    </FlexItem>
));

const LoginButton = withStyles(styles)(({classes, onClick, value}) => (
    <FlexItem>
        <Button variant="raised" color="primary" className={classes.button} onClick={onClick}>
            {value}
        </Button>
    </FlexItem>
));

const EmailNotVerify = withStyles(styles)(({open, transition=Transition, handleClose, handleSendVerifyEmail, email}) => (
    <Dialog
        open={open}
        transition={transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
    >
        <DialogTitle id="alert-dialog-slide-title">
            Email Verification Required
        </DialogTitle>
        <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
                {`please verify you account, we have sent an email to ${email}.`}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            {/*<Button onClick={handleSendVerifyEmail} color="primary">*/}
                {/*Resend Verification Email*/}
            {/*</Button>*/}
            <Button onClick={handleClose} color="primary">
                Close
            </Button>
        </DialogActions>
    </Dialog>
));

const ErrorMessageBox = ({transition=Transition, open, handleClose, message, header}) => (
    <Dialog
        open={open}
        transition={transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
    >
        <DialogTitle id="alert-dialog-slide-title">
            {header}
        </DialogTitle>
        <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
                {message}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose} color="primary">
                Close
            </Button>
        </DialogActions>
    </Dialog>
);

class Login extends Component {

    state = {
        email: "",
        password: "",
        showEmailVerifyPopUp: false,
        showLoginPopUp: false,
        errorMessage: "",
    };

    static propTypes = {

    };

    handleChange = (field) => (e) => {
        this.setState({ [field]: e.target.value })
    };

    openEmailConfirmDialog = () => {
        this.setState({
            showEmailVerifyPopUp: true
        });
    };


    closeEmailConfirmDialog = () => {
        this.setState({
            showEmailVerifyPopUp: false,
            errorMessage: ""
        });
    };

    openErrorDialog = (msg) => {
        this.setState({
            showLoginPopUp: true,
            errorMessage: msg,
        });
    };

    closeErrorDialog = () => {
        this.setState({ showLoginPopUp: false });
    };

    resendVerifyEmail = () => {
        auth.currentUser.sendEmailVerification()
            .then(() => console.log("email sent"))
            .catch(error => console.log(error))
    };

    loginHandler = () => {
        const {email, password} = this.state;

        auth.signInWithEmailAndPassword(email, password)
            .then(user => {
                const {emailVerified} = user;
                if (emailVerified === false){
                    this.openEmailConfirmDialog();
                    user.sendEmailVerification();
                    this.props.context.auth.signOut();
                }
            })
            .catch(err => {
                this.openErrorDialog(err.message);
            })
    };

    facebookLoginHandler = () => {

        if (!auth.currentUser) {
            const provider = new firebase.auth.FacebookAuthProvider();

            auth.signInWithPopup(provider).then((result) => {
                // const token = result.credential.accessToken;
                // const user = result.user;

            }).catch((error) => {
                const errorCode = error.code;
                // const errorMessage = error.message;
                // const email = error.email;
                // const credential = error.credential;

                if (errorCode === 'auth/account-exists-with-different-credential') {
                    // alert('You have already signed up with a different auth provider for that email.');
                    // If you are using multiple auth providers on your app you should handle linking
                    // the user's accounts here.

                    this.openErrorDialog('You have already signed up with a different auth provider for that email.');
                } else {
                    this.openErrorDialog(error.message);
                }
            });
        } else {
            auth.signOut();
        }
    };

    googleLoginHandler = () => {

        // const { firebase: {auth, obj: firebase }} = this.props.context;
        // const {firebase, auth} = this.props.context

        if (!auth.currentUser) {
            const provider = new firebase.auth.GoogleAuthProvider();

            auth.signInWithPopup(provider).then((result) => {
                // const token = result.credential.accessToken;
                // const user = result.user;

            }).catch(function(error) {
                const errorCode = error.code;
                // const errorMessage = error.message;
                // const email = error.email;
                // const credential = error.credential;

                if (errorCode === 'auth/account-exists-with-different-credential') {
                    // alert('You have already signed up with a different auth provider for that email.');
                    this.openErrorDialog('You have already signed up with a different auth provider for that email.');
                } else {
                    this.openErrorDialog(error.message);
                }
            });
        } else {
            auth.signOut();
        }
    };

    render(){

        // const { classes } = this.props;
        const { email, password, showEmailVerifyPopUp, showLoginPopUp, errorMessage } = this.state;

        return (
            <Background>
                <GridContainer>
                    <UsernameField onChange={this.handleChange('email')} value={email} />
                    <PasswordField onChange={this.handleChange('password')} value={password} />
                </GridContainer>
                <GridContainer>
                    <LoginButton value={"Sign In"} onClick={this.loginHandler} />
                    <LoginButton value={"Sign Up"} onClick={() => this.props.history.push('/signup')} />
                    <LoginButton value={"Forget Password"} onClick={() => this.props.history.push('/resetpassword')} />
                </GridContainer>
                <GridContainer>

                    <LoginButton value={"Facebook Sign in"} onClick={this.facebookLoginHandler} />
                    <LoginButton value={"Google Sign in"} onClick={this.googleLoginHandler} />
                </GridContainer>
                <ErrorMessageBox open={showLoginPopUp} handleClose={this.closeErrorDialog} header="There was an error..." message={errorMessage} />
                <EmailNotVerify open={showEmailVerifyPopUp} handleClose={this.closeEmailConfirmDialog} handleSendVerifyEmail={this.resendVerifyEmail} email={email}/>
            </Background>
        );
    }
}

export default compose(
    withStyles(styles),
    withContext
) (Login);