import React from 'react';
import Paper from 'material-ui/Paper';
import {withStyles} from "material-ui/styles";

const styles = (theme) => ({

    paper: {
        padding: theme.spacing.unit * 2,
        marginTop: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
        marginBottom: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

const Background = ({children, classes, elevation=4}) => (

    <Paper className={classes.paper} elevation={elevation}>
        {children}
    </Paper>

)

export default withStyles(styles)(Background);